__author__ = "Adam"
__date__ = "19-May-2017 12:12:00 PM"

from distutils.core import setup

setup(
        name='Phidgets',
        version='2.1.9',
        description='Python Wrapper library for the Phidgets API, updated to support python3',
        author='Adam',
        author_email='adam@phidgets.com',
        url='http://www.phidgets.com',
        packages=['Phidgets', 'Phidgets.Devices', 'Phidgets.Events'],
        license='Creative Commons Attribution 2.5 Canada License',
)
